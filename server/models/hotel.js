var mongoose = require( 'mongoose' );

var hotelSchema = new mongoose.Schema({
    id : {
      type : String,
      required: true,
      unique: true
    },
    name: {
      type : String,
      required: true
    },
    stars: {
      type : Number,
      required: true
    },
    price: {
      type : Number,
      required: true
    },
    image : {
      type : String,
      required: true
    },
    amenities: [String]
});

module.exports = mongoose.model('Hotel', hotelSchema);
