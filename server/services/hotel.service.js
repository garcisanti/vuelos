var express = require('express');
var hotelController = require('../controllers/hotel.controller');

exports.getPage = function(req, res){
    hotelController.getPage(req.params.page, req.params.size)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.getHotelsByName = function(req, res){
    hotelController.getHotelsByName(req.params.name)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.getHotelsByStars = function(req, res){
    hotelController.getHotelsByStars(req.params.stars)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

//MONGO DB

exports.getList = function(req, res){
    hotelController.getList(req.params.page, req.params.size)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.getList = (page, size) => {
    return Hotel.find().exec();
}

exports.create = function(req, res){
    hotelController.create(req.body.hotel)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.remove = function(req, res){
    hotelController.remove(req.body.hotel)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}

exports.update = function(req, res){
    hotelController.update(req.body.hotel)
      .then(hotels => res.send(hotels))
      .catch(err => res.status(400).send('No se pueden obtener los hoteles: '+err));
}
