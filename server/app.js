// Get dependencies
require('rootpath')();
const compression = require('compression')
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const helmet = require('helmet');

// Get our API routes
const api = require('./routes/api');

//DB connection
require('./db');

const app = express();

// compress all responses
app.use(compression());

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, '../dist')));

app.use(helmet());

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist/index.html'));
});

module.exports = app;
