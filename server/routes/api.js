const express = require('express');
const router = express.Router();

const hotelService = require('../services/hotel.service');

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

router.get('/hotels/:page&:size', hotelService.getPage);
router.get('/hotel-name/:name', hotelService.getHotelsByName);
router.get('/hotel-stars/:stars', hotelService.getHotelsByStars);

router.get('/hotels/:page&:size', hotelService.getList);
router.post('/hotels/', hotelService.create);
router.put('/hotels/', hotelService.update);
router.delete('/hotels/', hotelService.remove);


module.exports = router;
