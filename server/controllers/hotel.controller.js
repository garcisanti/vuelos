const mongo = require('mongoose');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');
var Hotel = require('../models/hotel');

//LOCAL DATA

exports.getPage = function(page, size){
    var obj;
    var filePath = path.join(__dirname, '../data/data.json');
    return fs.readFileAsync(filePath, 'utf8');
}

exports.getHotelsByName = (name) => {
    var filePath = path.join(__dirname, '../data/data.json');
    return fs.readFileAsync(filePath, 'utf8')
            .then((data)=>{
                const hotels = JSON.parse(data);
                const hotelsFiltered = []
                for (let hotel of hotels) {
                    if(hotel.name.toLowerCase().indexOf(name) != -1){
                        hotelsFiltered.push(hotel);
                    }
                }
                return Promise.resolve(hotelsFiltered);
            })
}

exports.getHotelsByStars = (starsStr) => {
    var filePath = path.join(__dirname, '../data/data.json');
    return fs.readFileAsync(filePath, 'utf8')
            .then((data)=>{
                const hotels = JSON.parse(data);
                const hotelsFiltered = []
                const stars = starsStr.split('&');
                for (let hotel of hotels) {
                    for (let star of stars) {
                        if(hotel.stars == star)
                        {
                            hotelsFiltered.push(hotel);
                            break;
                        }
                    }
                }
                return Promise.resolve(hotelsFiltered);
            })
}

//MLAB MongoDB CRUD

exports.getList = (page, size) => {
    return Hotel.find().exec();
}

exports.create = (hotel) => {
    const newHotel = new Hotel(hotel);
    return newHotel.save();
}

exports.remove = (hotel) => {
    return Hotel.findOneAndRemove({id:hotel.id}).exce();
}

exports.update = (hotel) => {
    return Hotel.updateOne({id:hotel.id}, hotel).exce();
}
