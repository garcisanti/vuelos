# Almundo

![alt text](/images/desktop.png)
![alt text](/images/mobile.png)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Install dependencies

Run `yarn or npm install`

## Run server

Run `node server`. Navigate to `http://localhost:3000/`.

## MLAB MongoFB

Connected to MLAB database ready to CRUD services.

## Autor

Santiago García Suarez
