import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

@Injectable()
export class HotelService {

    // Observable array source
    private hotelsSource = new BehaviorSubject([]);

    // Observable string streams
    hotels$ = this.hotelsSource.asObservable();

    constructor(private http: Http) { }

    public getJSON(): Observable<any> {
        return this.http.get('api/hotels/:10&:10')
            .map((res) => res.json());
    }

    public filterByName(name: string): Observable<any> {
        return this.http.get('api/hotel-name/' + name)
            .map((res) => res.json());
    }

    public filterByStars(stars: string): Observable<any> {
        return this.http.get('api/hotel-stars/' + stars)
            .map((res) => res.json());
    }

     public setHotels(hotels) {
         this.hotelsSource.next(hotels);
     }
}
