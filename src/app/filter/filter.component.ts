import { Component, OnInit } from '@angular/core';
import { HotelService } from '../services/hotel.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

    public stars: boolean [];

    constructor(
      private hotelService: HotelService
    ) { }

    ngOnInit() {
      this.stars = [false, false, false, false, false];
    }

    public filterByStars() {
      const sum = this.stars[0] || this.stars[1] || this.stars[2] || this.stars[3] || this.stars[4];
      if ( !sum ) {
          this.hotelService.getJSON()
          .subscribe(
              next => {
                  this.hotelService.setHotels(next);
              },
              err => {
                  console.log(err);
              }
          );
      } else {
          let starsStr = '';
          let i = 1;
          for (const star of this.stars) {
              if (star) {
                  if ( starsStr === '' ) {
                      starsStr += i;
                  }
                  else {
                      starsStr = i  + '&' + starsStr;
                  }
              }
              i++;
          }
          this.hotelService.filterByStars(starsStr)
          .subscribe(
              next => {
                  this.hotelService.setHotels(next);
              },
              err => {
                  console.log(err);
              }
          );
      }
    }

    public filterByName(name: string) {
      if (name === '') {
          this.hotelService.getJSON()
          .subscribe(
              next => {
                  this.hotelService.setHotels(next);
              },
              err => {
                  console.log(err);
              }
          );
      } else {
          this.hotelService.filterByName(name)
          .subscribe(
              next => {
                  this.hotelService.setHotels(next);
              },
              err => {
                  console.log(err);
              }
          );
      }
    }

}
