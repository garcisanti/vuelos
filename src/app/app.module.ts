import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HotelsComponent } from './hotels/hotels.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FilterComponent } from './filter/filter.component';

import {HotelService} from './services/hotel.service';


@NgModule({
    declarations: [
        AppComponent,
        HotelsComponent,
        ToolbarComponent,
        FilterComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule
    ],
    providers: [
        HotelService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
