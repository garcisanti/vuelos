import { Component, OnInit } from '@angular/core';
import { HotelService } from '../services/hotel.service';
import { Hotel } from '../models/hotel';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

    public hotels: Hotel[];

    constructor(private hotelService: HotelService) {
        this.hotelService.hotels$.subscribe(
            hotels => this.hotels = hotels
        );
    }

  ngOnInit() {
      this.hotels = [];
      this.hotelService.getJSON().subscribe(
          next => {
              this.hotelService.setHotels(next);
          },
          err => {
              console.log(err);
          }
      );
  }

}
